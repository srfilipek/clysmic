//
//  AppDelegate.swift
//  clysmic
//
//  Created by Stefan on 7/10/16.
//  Copyright © 2016 Stefan R. Filipek. All rights reserved.
//

import Cocoa
import AppKit
import ApplicationServices


// Menu element container for programatic initialization
let menuElements = [
    ("Snap Left",
        #selector(AppDelegate.snapLeftMenuWrapper),
        "", //NSString(characters: [unichar(NSLeftArrowFunctionKey)], length: 1),
        NSEvent.ModifierFlags.control.union(NSEvent.ModifierFlags.option)),
    
    ("Snap Right",
        #selector(AppDelegate.snapRightMenuWrapper),
        "", //NSString(characters: [unichar(NSRightArrowFunctionKey)], length: 1),
        NSEvent.ModifierFlags.control.union(NSEvent.ModifierFlags.option)),
    
    ("Snap Up",
        #selector(AppDelegate.snapUpMenuWrapper),
        "", //NSString(characters: [unichar(NSUpArrowFunctionKey)], length: 1),
        NSEvent.ModifierFlags.control.union(NSEvent.ModifierFlags.option)),
    
    ("Snap Down",
        #selector(AppDelegate.snapDownMenuWrapper),
        "", //NSString(characters: [unichar(NSDownArrowFunctionKey)], length: 1),
        NSEvent.ModifierFlags.control.union(NSEvent.ModifierFlags.option)),
    
    ("Fullscreen",
        #selector(AppDelegate.fullscreenMenuWrapper),
        "", //"f",
        NSEvent.ModifierFlags.control.union(NSEvent.ModifierFlags.option)),
    
    ("---",
        #selector(AppDelegate.nilWrapper),
        "",
        NSEvent.ModifierFlags(rawValue: 0)),
    
    ("Quit",
        #selector(AppDelegate.quit),
        "",
        NSEvent.ModifierFlags(rawValue: 0)),
]


// The main class that performs the necessary window manipulation
open class WindowService: NSObject {
    
    // Returns the active window, or nil if none
    func getActiveWindow() -> AXUIElement? {
        let foreApp = NSWorkspace.shared.frontmostApplication!
        
        NSLog("foreApp = \(String(describing: foreApp.localizedName))")
        
        let foreElement = AXUIElementCreateApplication(foreApp.processIdentifier)
        
        var anyObjectWindow: AnyObject?
        AXUIElementCopyAttributeValue(
            foreElement, kAXFocusedWindowAttribute as CFString, &anyObjectWindow)
        
        NSLog("window = \(String(describing: anyObjectWindow))")
        
        if anyObjectWindow == nil {
            return nil
        }
        
        return (anyObjectWindow as! AXUIElement)
    }

    // Returns the CGRect to use for window positioning
    //
    // - returns: A CGRect
    //
    // IMPORTANT NOTE ON COORDINATE SYSTEMS
    //
    // Because the OS interface was designed so brilliantly, we have to worry
    // about two different coordinate systems at play. The .vislbleFrame and
    // .frame origins are associated with a 0,0 at the BOTTOM left of the "main"
    // screen (note that main() isn't actually main... awesome). The cordinate
    // system used to actually POSITION a window has 0,0 at the UPPER left (via
    // AXUIElementSetAttributeValue and the kAXPositionAttribute). Also, the y
    // axis is flipped. Genius.
    //
    // This returns a CGRect with the origin relative to the UPPER LEFT of the
    // active screen and with the positive y axis pointing down. This way we can
    // use it directly to position windows.
    //
    // Coordinate system of .visibleFrame and .frame:
    //
    //        +y
    //        |
    //        |
    //        |,- given origin, bottom left of screen
    //         --------- +x
    //
    // Coordinate system of kAXPositionAttribute:
    //
    //         --------- +x
    //        |`- origin reference, upper left of the screen
    //        |
    //        |
    //        +y
    //
    // But wait! There's more!
    //
    // With a multi-screen scenario, it becomes an even larger challenge!
    //
    // Say you have two screens side by side with the main screen on the right.
    // The left screen is taller than the right and their bottoms are offset:
    //
    // Coordinate system given via NSScreen .frame and .visibleFrame:
    //      +y (secondary)
    //       ---------------
    //      |               |(screen[0])
    //      |               |-----------
    //      |               |           |
    //      |               |           |
    //      |               |0,0        |
    //      |-width,origin.y|-----------
    //       ---------------
    //
    // Coordinate system we need to use for window positioning:
    //      -y (secondary)
    //       ---------------             ---
    //      |-width,-??     |(screen[0])  | Need to calculate this offset
    //      |               |----------- ---
    //      |-width,0       |0,0        |
    //      |               |           |
    //      |               |           |
    //      |               |-----------
    //       ---------------
    //
    // Oh, and we might have optional menu bars and docks to deal with too. Yay!
    //
    //      +y
    //       ---------------    ---
    //      |   menu bar    |    |frame.height
    //      |d             d|    | ---
    //      |o             o|    |  |
    //      |c             c|    |  |
    //      |k             k|    |  | visibleFrame.height
    //      |?             ?|    | --- visibleFrame.origin.y (above dock)
    //      |     dock?     |    |
    //       --------------- +x ---
    //       `-frame.origin
    //      |---------------| frame.width
    //        |-----------| visibleFrame.width (may change if side dock used)
    //        `visibleFrame.origin.x (offset if dock is on the left)
    // 
    // With the above layouts, we can calculate the dock height and then the
    // menu bar height. The dock height seems to be "4" when auto-hide is on
    // (some extra padding by the OS). The menu bar height will be zero when
    // it auto-hides.
    //
    // This function returns an origin that compensates for everything,
    // including a menu bar. This way windows will be easily placed between a
    // menu bar and dock, if any of them exist in any position.
    //
    func getScreenRect() -> CGRect {
        // Get the active screen frame bounds
        let visibleFrame = NSScreen.main!.visibleFrame
        let frame        = NSScreen.main!.frame
        
        // Get the main screen frame bounds (not main(), but screen 0)
        let mainFrame    = NSScreen.screens[0].frame
        
        NSLog("visibleFrame = \(visibleFrame)")
        NSLog("frame = \(frame)")
        NSLog("mainFrame = \(mainFrame)")
        
        // Calculate if we have a dock and/or menu bar in our screen
        let dockHeight    = visibleFrame.origin.y - frame.origin.y
        let menuBarHeight = frame.height - (visibleFrame.height + dockHeight)
        
        // Note: Side docks are automatically dealt with when using
        // visibleFrame.origin.x and visibleFrame.width.
        
        // Calculate our upper left y origin using the mainFrame dimensions.
        // Assume 0,0 is the upper left of the main screen, and just walk our
        // way to the upper left of the secondary screen (below the menu bar)
        // where positive is down and negative is up.
        let originY = mainFrame.height + (mainFrame.origin.y - frame.origin.y) -
            frame.height + menuBarHeight
        
        let windowRect = CGRect(x: visibleFrame.origin.x,
                                y: originY,
                                width: visibleFrame.width,
                                height: visibleFrame.height)
        
        NSLog("windowRect = \(windowRect)")
        
        return windowRect
    }
    
    // Snap the active window to the left side of its screen
    func snapLeft() {
        let potentialWindow = getActiveWindow()
        if potentialWindow == nil {
            return
        }
        
        let activeWindow = potentialWindow!
        let windowFrame  = getScreenRect()
        
        move(activeWindow,
             x: windowFrame.origin.x, y: windowFrame.origin.y,
             width: windowFrame.width/2, height: windowFrame.height)
    }
    
    // Snap the active window to the right side of its screen
    func snapRight() {
        let potentialWindow = getActiveWindow()
        if potentialWindow == nil {
            return
        }
        
        let activeWindow = potentialWindow!
        let windowFrame  = getScreenRect()
        
        move(activeWindow,
             x: windowFrame.origin.x + windowFrame.width/2,
             y: windowFrame.origin.y,
             width: windowFrame.width/2,
             height: windowFrame.height)
    }
    
    // Snap the active window to the top of its screen
    func snapUp() {
        let potentialWindow = getActiveWindow()
        if potentialWindow == nil {
            return
        }
        
        let activeWindow = potentialWindow!
        let windowFrame  = getScreenRect()
        
        move(activeWindow,
             x: windowFrame.origin.x,
             y: windowFrame.origin.y,
             width: windowFrame.width,
             height: windowFrame.height/2)
    }
    
    // Snap the active window to the bottom of its screen
    func snapDown() {
        let potentialWindow = getActiveWindow()
        if potentialWindow == nil {
            return
        }
        
        let activeWindow = potentialWindow!
        let windowFrame  = getScreenRect()
        
        move(activeWindow,
             x: windowFrame.origin.x,
             y: windowFrame.origin.y + windowFrame.height/2,
             width: windowFrame.width,
             height: windowFrame.height/2)
    }
    
    // Make the active window take up its entire screen
    func fullscreen() {
        let potentialWindow = getActiveWindow()
        if potentialWindow == nil {
            return
        }
        
        let activeWindow = potentialWindow!
        let windowFrame  = getScreenRect()
        
        move(activeWindow,
             x: windowFrame.origin.x,
             y: windowFrame.origin.y,
             width: windowFrame.width,
             height: windowFrame.height)
    }
    
    // Move a window to a given coordinate and resize with a given size
    func move(_ window: AXUIElement,
              x: CGFloat, y: CGFloat,
              width: CGFloat, height: CGFloat) {
        // Translate inputs to AXValue objects (first through CGxxx objects)
        var size = CGSize(width: width, height: height)
        var pos  = CGPoint(x: x, y: y)
        
        let sizeAnyObject = AXValueCreate(
            AXValueType(rawValue: kAXValueCGSizeType)!, &size)!
        let posAnyObject  = AXValueCreate(
            AXValueType(rawValue: kAXValueCGPointType)!, &pos)!
        
        NSLog("size = \(size)")
        NSLog("pos = \(pos)")
        
        // Move and reposition
        AXUIElementSetAttributeValue(
            window, kAXPositionAttribute as CFString, posAnyObject)
        AXUIElementSetAttributeValue(
            window, kAXSizeAttribute as CFString, sizeAnyObject)
    }
    
    // Accessibility interface main entry point
    @objc
    open func handleService(
        _ pboard: NSPasteboard,
        userData: String,
        error: AutoreleasingUnsafeMutablePointer<NSString?>) {

        NSLog("Service responding to \(userData)")
        
        switch userData {
        case "snapUp":
            self.snapUp()
            
        case "snapDown":
            self.snapDown()
            
        case "snapLeft":
            self.snapLeft()
            
        case "snapRight":
            self.snapRight()
            
        case "fullscreen":
            self.fullscreen()
            
        default:
            NSLog("Unknown service request")
            
        }
    }
}

// Main application
@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    var statusItem: NSStatusItem
    var windowService: WindowService

    override init() {
        self.statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
        self.windowService = WindowService()
        
        super.init()
    }
    
    @objc func quit() {
        NSRunningApplication.current.terminate()
    }
    
    func setupMenuElements() {
        // Setup the menu element with an icon
        self.statusItem.button!.image = NSImage(named: NSImage.Name(rawValue: "StatusIcon"))
        self.statusItem.button!.image!.isTemplate = true
        self.statusItem.menu = NSMenu(title: "clysmic")
        
        self.statusItem.highlightMode = true
        
        // Setup the sub menus with key shortcuts
        for (title, selector, shortcut, mask) in menuElements {
            var menuItem: NSMenuItem
            
            if (title != "---") {
                menuItem = NSMenuItem(
                    title: title, action: selector,
                    keyEquivalent: shortcut as String)
            } else {
                menuItem = NSMenuItem.separator()
            }
            menuItem.keyEquivalentModifierMask = mask
            self.statusItem.menu?.addItem(menuItem)
        }
    }
    
    func setupServices() {
        NSApp.servicesProvider = self.windowService
        NSUpdateDynamicServices()
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        setupMenuElements()
        //setupServices()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    @objc func snapLeftMenuWrapper(_ sender: NSMenuItem) {
        self.windowService.snapLeft()
    }
    
    @objc func snapRightMenuWrapper(_ sender: NSMenuItem) {
        self.windowService.snapRight()
    }
    
    @objc func snapUpMenuWrapper(_ sender: NSMenuItem) {
        self.windowService.snapUp()
    }
    
    @objc func snapDownMenuWrapper(_ sender: NSMenuItem) {
        self.windowService.snapDown()
    }
    
    @objc func fullscreenMenuWrapper(_ sender: NSMenuItem) {
        self.windowService.fullscreen()
    }
    
    @objc func nilWrapper(_ sender: NSMenuItem) {
        // Empty
    }
}

